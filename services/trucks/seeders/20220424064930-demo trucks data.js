'use strict';
const trucksData = require('../masterdata/trucks.json');
module.exports = {
  async up (queryInterface, Sequelize) {
    const trucksDataMapped = trucksData.map((eachElm) => {
      eachElm.createdAt = new Date();
      eachElm.updatedAt = new Date();

      return eachElm;
    })
    await queryInterface.bulkInsert('Trucks',trucksDataMapped,{});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Trucks',null,{});
  }
};
