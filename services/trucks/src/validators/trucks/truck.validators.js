const { body, query, param, validationResult } = require('express-validator')
const apiError = require('../../models/errorHandling/baseResponse.error');
const constMsg = require('../../constant/constant');

const create = () => {
  return [
    // Sanitize fields to integer value
    body(['number_of_gears','number_of_tires']).toInt(),
    // Validators
    body(['name','code'],constMsg.IS_EMPTY_MESSAGE).notEmpty(),
    body(['number_of_gears','number_of_tires'],constMsg.IS_NUMBER_MESSAGE).isInt()
  ]
}

const idParam = () => {
  return [
    param('truck_id',constMsg.IS_NUMBER_MESSAGE).isInt()
  ]
}
const search = () => {
  return [
    query('q').toLowerCase()
  ]
}
const validate = (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        if (errors.errors[0].msg === constMsg.IS_EMPTY_MESSAGE ||
            errors.errors[0].msg === constMsg.IS_NUMBER_MESSAGE) {
            throw new apiError.BadRequest({errors : errors.array()});
        }
    }
    next();
  } catch (error) {
      next(error)
  }
}

module.exports = {
  create,
  validate,
  idParam,
  search
}