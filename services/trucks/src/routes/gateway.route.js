const express = require('express');
const router = express.Router();
const truckRouter = require('./Trucks/trucks.route');
const truckControllers = require('../controllers/Trucks/trucks.controllers');
const truckValidators = require('../validators/trucks/truck.validators');
const paginationValidator = require('../validators/pagination/pagination.validators');

router.use('/trucks',truckRouter);
router.get('/search',paginationValidator,truckValidators.search(),truckControllers.search);

module.exports = router;