const express = require('express');
const router = express.Router();
const truckControllers = require('../../controllers/Trucks/trucks.controllers');
const truckValidators = require('../../validators/trucks/truck.validators');
const paginationValidator = require('../../validators/pagination/pagination.validators');

router.route('/')
    .get(paginationValidator,truckControllers.getAll)
    .post(truckValidators.create(),truckValidators.validate,truckControllers.create)
router.route('/:truck_id')
    .get(truckValidators.idParam(),truckValidators.validate,truckControllers.getById)
    .put(truckValidators.idParam(),truckValidators.create(),truckValidators.validate,truckControllers.update)
    .delete(truckValidators.idParam(),truckValidators.validate,truckControllers.remove);

module.exports = router;