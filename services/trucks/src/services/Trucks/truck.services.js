const { Truck } = require('../../../models');
const { pagination } = require('../../utils/pagination');
const apiError = require('../../models/errorHandling/baseResponse.error');
const { Op } = require('sequelize');
module.exports = {
    async search(req) {
        const {page,row,q} = req;
        const {page:pagePagination,row:rowPagination} = pagination(page,row);
        const options = {
            where : {
                "name" : {
                    [Op.iLike] : `%${q}%`
                } 
            },
            attributes : {
                exclude : ['createdAt','updatedAt']
            },
            limit : rowPagination,
            offset : pagePagination,
            order : [['id','ASC']]
        }
        return await Truck.findAll(options);
    },
    async getAll(req) {
        const {page,row} = pagination(req.page,req.row);
        const options = {
            attributes : {
                exclude : ['createdAt','updatedAt']
            },
            limit : row,
            offset : page,
            order : [['id','ASC']]
        }
        return await Truck.findAll(options);
    },
    async create(data) {
        const checkUniqueCode = await Truck.findOne({
            where : {
                "code" : data.code
            }
        })
        if(checkUniqueCode) {
            throw new apiError.BadRequest({errors : `Truck code must be unique, code with value ${data.code} was used`});
        }
        return await Truck.create(data);
    },
    async getById(id) {
        const result = await Truck.findByPk(id);
        if(!result){
            throw new apiError.NotFound({errors : `Truck with id ${id} not found`})
        }
        return result;
    },
    async remove(id) {
        const result = await Truck.destroy({where : {
            id
        }});
        if(!result){
            throw new apiError.NotFound({errors : `Truck with id ${id} not found`})
        }
        return result;
    },
    async update(id,data) {
        const result = await Truck.findByPk(id);
        if(!result){
            throw new apiError.NotFound({errors : `Truck with id ${id} not found`})
        }
        const checkUniqueCode = await Truck.findOne({
            where : {
                "code" : data.code
            }
        })
        if(checkUniqueCode) {
            throw new apiError.BadRequest({errors : `Truck code must be unique, code with value ${data.code} was used`});
        }
        const options = {
            where : {
                id
            }
        };
        
        return await Truck.update(data,options);;
    }

}