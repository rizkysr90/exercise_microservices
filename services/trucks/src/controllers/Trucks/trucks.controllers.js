const baseResponse = require('../../models/baseResponse/baseResponse');
const truckServices = require('../../services/Trucks/truck.services');

module.exports = {
    async search(req,res,next) {
        try {
            const result = await truckServices.search(req.query);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    },
    async getAll(req,res,next) {
        try{
            const result = await truckServices.getAll(req.query);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    },
    async create(req,res,next) {
        try{
            const result = await truckServices.create(req.body);
            res.status(201).json(baseResponse.created(result));
        } catch (error) {
            next(error)
        }
    },
    async getById(req,res,next) {
        try{
            const result = await truckServices.getById(req.params.truck_id);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    },
    async remove(req,res,next) {
        try{
            const result = await truckServices.remove(req.params.truck_id);
            res.status(200).json(baseResponse.deleted(result));
        } catch (error) {
            next(error)
        }
    },
    async update(req,res,next) {
        try{
            const result = await truckServices.update(req.params.truck_id,req.body);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    }
}