'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Cars', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      code: {
        type: Sequelize.STRING,
        unique : true
      },
      number_of_gears: {
        type: Sequelize.INTEGER
      },
      number_of_tires: {
        type: Sequelize.INTEGER
      },
      car_type_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references : {
          model : "Car_types",
          key : "id"
        },
        onUpdate : 'CASCADE',
        onDelete : 'SET NULL'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Cars');
  }
};