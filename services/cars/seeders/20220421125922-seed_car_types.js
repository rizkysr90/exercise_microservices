'use strict';
const carTypeData = require('../masterdata/carTypes.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const mapped = carTypeData.map((eachType) => {
      eachType.createdAt = new Date();
      eachType.updatedAt = new Date();

      return eachType;
    })
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Car_types',mapped);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Car_types',null,{});
  }
};
