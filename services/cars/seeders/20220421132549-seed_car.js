'use strict';
const carData = require('../masterdata/car.json');
module.exports = {
  async up (queryInterface, Sequelize) {
    const mapped = carData.map((eachCar) => {
      eachCar.createdAt = new Date();
      eachCar.updatedAt = new Date();

      return eachCar;
    })
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
      await queryInterface.bulkInsert('Cars',mapped);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Cars',null,{});
  }
};
