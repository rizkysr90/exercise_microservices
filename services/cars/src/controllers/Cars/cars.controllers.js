const baseResponse = require('../../models/baseResponse/baseResponse');
const carServices = require('../../services/Cars/car.services');

module.exports = {
    async getAll(req,res,next) {
        try{
            const result = await carServices.getAll(req.query);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    },
    async create(req,res,next) {
        try{
            const result = await carServices.create(req.body);
            res.status(201).json(baseResponse.created(result));
        } catch (error) {
            next(error)
        }
    },
    async getById(req,res,next) {
        // console.log('Here in by id');
        try{
            const result = await carServices.getById(req.params.car_id);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    },
    async remove(req,res,next) {
        try{
            const result = await carServices.remove(req.params.car_id);
            res.status(200).json(baseResponse.deleted(result));
        } catch (error) {
            next(error)
        }
    },
    async update(req,res,next) {
        try{
            const result = await carServices.update(req.params.car_id,req.body);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    },
    async search(req,res,next) {
        console.log(req.query);
        try {
            const result = await carServices.search(req.query);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    }
}