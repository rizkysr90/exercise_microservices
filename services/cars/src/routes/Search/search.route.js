const express = require('express');
const router = express.Router();
const paginationValidator = require('../../validators/pagination/pagination.validators');
const carControllers = require('../../controllers/Cars/cars.controllers');
const carValidators = require('../../validators/cars/car.validators');

router.route('/')
    .get(paginationValidator,carValidators.search(),carControllers.search);


module.exports = router;