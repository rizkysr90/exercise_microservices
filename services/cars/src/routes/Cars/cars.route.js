const express = require('express');
const router = express.Router();
const carControllers = require('../../controllers/Cars/cars.controllers');
const carValidators = require('../../validators/cars/car.validators');
const paginationValidator = require('../../validators/pagination/pagination.validators');
// router.get('/search',paginationValidator,carValidators.search,carControllers.search);
router.route('/')
    .get(paginationValidator,carControllers.getAll)
    .post(carValidators.create(),carValidators.validate,carControllers.create)
router.route('/:car_id')
    .get(carValidators.idParam(),carValidators.validate,carControllers.getById)
    .put(carValidators.idParam(),carValidators.create(),carValidators.validate,carControllers.update)
    .delete(carValidators.idParam(),carValidators.validate,carControllers.remove);

module.exports = router;