const express = require('express');
const router = express.Router();
const carRouters = require('./Cars/cars.route');
const searchRouters = require('./Search/search.route');

router.use('/cars',carRouters);
router.use('/search',searchRouters);
module.exports = router;