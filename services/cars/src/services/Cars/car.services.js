const {Car,Car_type} = require('../../../models');
const { pagination } = require('../../utils/pagination');
const apiError = require('../../models/errorHandling/baseResponse.error');
const { Op } = require('sequelize');
module.exports = {
    async getAll(req) {
        const {page,row} = pagination(req.page,req.row);
        const options = {
            attributes : {
                exclude : ['createdAt','updatedAt']
            },
            limit : row,
            offset : page,
            order : [['id','ASC']]
        }
        return await Car.findAll(options);
    },
    async create(data) {
        const checkForeignKey = await Car_type.findByPk(data.car_type_id);
        const checkUniqueCode = await Car.findOne({
            where : {
                "code" : data.code
            }
        })
        if(checkUniqueCode) {
            throw new apiError.BadRequest({errors : `Car code must be unique, code with value ${data.code} was used`});
        }
        if(!checkForeignKey) {
            throw new apiError.NotFound({errors : `Car_type with id ${data.car_type_id} not found`})
        };
        return await Car.create(data);
    },
    async getById(id) {
        const result = await Car.findByPk(id);
        if(!result){
            throw new apiError.NotFound({errors : `Car with id ${id} not found`})
        }
        return result;
    },
    async remove(id) {
        const result = await Car.destroy({where : {
            id
        }});
        if(!result){
            throw new apiError.NotFound({errors : `Car with id ${id} not found`})
        }
        return result;
    },
    async update(id,data) {
        const result = await Car.findByPk(id);
        if(!result){
            throw new apiError.NotFound({errors : `Car with id ${id} not found`})
        }
        const checkForeignKey = await Car_type.findByPk(data.car_type_id);
        if(!checkForeignKey) {
            throw new apiError.NotFound({errors : `Car_type with id ${data.car_type_id} not found`})
        };
        const options = {
            where : {
                id
            }
        };
        
        return await Car.update(data,options);;
    },
    async search(req) {
        const {page,row,q} = req;
        const {page:pagePagination,row:rowPagination} = pagination(page,row);
        const options = {
            where : {
                "name" : {
                    [Op.iLike] : `%${q}%`
                } 
            },
            attributes : {
                exclude : ['createdAt','updatedAt']
            },
            limit : rowPagination,
            offset : pagePagination,
            order : [['id','ASC']]
        }
        return await Car.findAll(options);
    }

}