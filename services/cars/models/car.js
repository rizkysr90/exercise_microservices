'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const carTypeModel = models.Car_type;
      const carModel = models.Car;

      carTypeModel.hasMany(carModel,{
        foreignKey: "car_type_id"
      });
      carModel.belongsTo(carTypeModel,{
        foreignKey: "car_type_id"
      });
    }
  }
  Car.init({
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    number_of_gears: DataTypes.INTEGER,
    number_of_tires: DataTypes.INTEGER,
    car_type_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Car',
  });
  return Car;
};