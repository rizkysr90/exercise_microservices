const express = require('express');
const app = express();
const gatewayRouter = require('./src/routes/gateway.route');

app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use('/',gatewayRouter);
app.all('*',(req,res) => {
    res.status(404).json('URL NOT FOUND');
});

app.use((err, req, res, next) => {
    console.log(err);
    const responseBody = {
        title : err.name || "500 - Internal Server Error",
        message : err.message || 'The server encountered an unexpected condition that prevented it from fulfilling the request',
        errors : err.errors || []
    }
    
    res.status(err.statusCode || 500).json({
        errors : responseBody
    });
    
});
app.listen(3000,() => {
    console.log(`Service Cars Listening at http:localhost:3000`)
});

