const router = require('express').Router();
const truckControllers = require('../../controllers/trucks/truck.controllers');

router.route('/search')
    .get(truckControllers.search)
router.route('/')
    .get(truckControllers.getAll)
    .post(truckControllers.create)
router.route('/:truck_id')
    .get(truckControllers.getById)
    .put(truckControllers.update)
    .delete(truckControllers.remove)
module.exports = router;