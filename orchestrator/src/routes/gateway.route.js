const router = require('express').Router();
const mainRouter = require('./main/main.route.js');

router.use('/feugeseot',mainRouter);

module.exports = router;