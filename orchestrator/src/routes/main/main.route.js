const router = require('express').Router();
const carRouters = require('../cars/car.route');
const truckRouters = require('../trucks/truck.route');
router.use('/trucks',truckRouters);
router.use('/cars',carRouters);

module.exports = router;