const router = require('express').Router();
const carControllers = require('../../controllers/cars/car.controllers');

router.route('/search')
    .get(carControllers.search)
router.route('/')
    .get(carControllers.getAll)
    .post(carControllers.create)
router.route('/:car_id')
    .get(carControllers.getById)
    .put(carControllers.update)
    .delete(carControllers.remove)
module.exports = router;