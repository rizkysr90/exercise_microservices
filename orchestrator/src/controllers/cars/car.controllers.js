const axios = require('axios').default;
const urlPlural = 'http://localhost:3000/cars';
const urlSingular = 'http://localhost:3000/car';
module.exports = {
    async getAll(req,res,next) {
        try {
            const isAnyQueryParam = Object.keys(req.query).length !== 0;
            const params = {...req.query};
            const result = await axios.get(`${urlPlural}`,isAnyQueryParam === true ? {params} : null)
                            .then(result => result.data);
            res.status(200).json(result);
        } catch (error) {
            next(error)
        }
    },
    async create(req,res,next) {
        try {
            const carPayload = {...req.body};
            const result = await axios.post(`${urlPlural}`,carPayload)
                .then(result => result.data);
            res.status(200).json(result);
        } catch (error) {
            next(error);
        }
    },
    async update(req,res,next) {
        try {
            const carPayload = {...req.body};
            const result = await axios.put(`${urlPlural}/${req.params.car_id}`,carPayload)
                .then(result => result.data);
            res.status(200).json(result);
        } catch (error) {
            next(error);
        }
    },
    async remove(req,res,next) {
        try {
            const result = await axios.delete(`${urlPlural}/${req.params.car_id}`)
                .then(result => result.data);
            res.status(200).json(result);

        } catch (error) {
            next(error)
        }
    },
    async getById(req,res,next) {
        try {
            const result = await axios.get(`${urlPlural}/${req.params.car_id}`)
            .then(result => result.data);
            res.status(200).json(result);
        } catch (error) {
            next(error);
        }
    },
    async search(req,res,next) {
        try {
            const isAnyQueryParam = Object.keys(req.query).length !== 0;
            const params = {...req.query};
            const result = await axios.get(`http://localhost:3000/search`,isAnyQueryParam === true ? {params} : null)
                            .then(result => result.data);

            res.status(200).json(result);
        } catch (error) {
            next(error)
        }
    }
}